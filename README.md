# reseau_entreprise
Le schema du groupe:  
![schema de groupe du network](./img/group_network.svg)  

Ce qui donne pour moi:  
![schema de mon point de vue](./img/network_callune.svg)
## Partie 1 Mise en place du réseau sur les VMs
### 1.1 Réseau privé de l'entreprise
* Installer Virtual box
* Télécharger l'iso debian 10.2 buster
* Créer la vm privé1
    hostname: private1 
    domain name: dnprivate1
    root pwd : rootpwd
    username: private1
    password: vmpassword
* Créer la vm privé2
    hostname: private2 
    domain name: dnprivate2
    root pwd : rootpwd
    username: private2
    password: vmpassword
* On va dans virtual box, configuration de privé1 et 2, réseau, on passe de nat a interne.
* `ip a` renvoit la configuration des cartes, on voit l'interface enp0s3.  
Sur cette interface on ajoute une adresse ip.  
`ip addr add 192.168.10.10/24 dev enp0s3`  
Attention cette commande n'est que temporaire, il vaut mieux modifier `/etc/network/interface `avec cet exemple de conf: 
```
auto enp0s3
    iface enp0s3 inet static
        address 192.168.10.10
        netmask 255.255.255.0
        gateway 192.168.10.254
```
Il faudra éventuellement utiliser `ifup enp0s3` pour relancer l'interface.
* On vérifie que les machines se ping entre elle.  
`ping 192.168.10.20` depuis 192.168.10.10 et inversement.

### 1.2 Réseau public de l'entreprise DMZ
* Créer la vm dmz (clone de private2)
* La configurer :
```
auto enp0s3
    iface enp0s3 inet static
        address 1.1.1.1
        netmask 255.255.255.0
        gateway 1.1.1.254
```
* On passe par l'interface de virtual box pour configurer une interface en nat afin d'obtenir l'accès a internet.
* `apt install apache2 curl`
* `systemctl start apache2` 
* On désactive l'interface nat
* On vérifie qu'apache2 marche bien avec un `curl localhost`

### 1.3 Interconnexion des réseaux de l'entreprise
* Créer la vm routeur (clone de private1)
* Y ajouter des interfaces internes grace à la configuration de virtualbox
* Configurer les 3 interfaces
```
auto enp0s3
    iface enp0s3 inet static
        address 1.1.1.254
        netmask 255.255.255.0

auto enp0s8
    iface enp0s3 inet static
        address 192.168.10.254
        netmask 255.255.255.0

auto enp0s9
    iface enp0s3 inet static
        address 200.200.200.1
        netmask 255.255.255.0
```
* Vérifier qu'on peut ping la DMZ et private1 depuis le routeur.
* Il faut maintenant activé l'option de routage pour que private puisse ping  DMZ avec `echo 1 > /proc/sys/net/ipv4/ip_foward`  
Pour une config permanante il faut editer `/etc/sysctl.conf` et ajouter cette ligne ` net.ipv4.ip_forward=1`.  
Ne pas oublier de redemarer le service avec `sysctl -p /etc/sysctl.conf`.
* Vérifier que la dmz puisse ping private et inversement. Il faudra peut être rafraichir les interface avec `ifdown` `ifup`.  
Si cela ne marche pas, vérifier toute les adresses sur la route, de la plus proche a la plus loin pour identifier la source du problème.

### Questions
1.1-
    Le réseau privé dans notre cas a forcément accès à son réseau public sans avoir besoin de nat, car ils sont directement connectés au même routeur. Et il n’y a pas de règles de sécurité encore.

1.2- 
Exemple d’un Curl envoyé depuis client privé 1 : @ip = 192.168.10.10 vers web : 2.2.2.2  

le client privé 1 ne connait pas de réseau permettant d’accéder au 2.2.2.2
il l’envoie donc à sa gateway : 192.168.10.254, pour connaître quelle @mac correspond à cette @ip, on utilise ARP.

on peut enfin envoyer notre syn tcp vers 2.2.2.2 via @mac gateway, la gateway connait comment accéder à la machine 2.2.2.2 car il est connecté au réseau 2.2.2.254 par son interface 2.2.2.254.

puis le serveur web répond via un synack, et le client fait son ack de finalisation de connexion tcp, accompagné de sa requête http.

Dans un cas classique, chez nous, notre machine posséde une addresse privée et sort via la gateway. Cela nécessite une opération de masquerading, qui permet à notre machine de communiquer à travers l’internet.


1.3- 
Les routes:
* D'une machine du réseau privé :   
default via 192.168.10.254
* D'un serveur de la DMZ :   
default via 1.1.1.254
* Du routeur:  
200.200.200.0 via enp0s9  
1.1.1.0 via enp0s8  
192.168.10.0 via enp0s3

## Partie 2 Interconnexcion avec le "reste du monde"
### Mise en place d'un routeur vers l'extérieur
* Utiliser la configuration de virtualbox pour que la 3eme interface soit `accès par pont enp0s31f6`
* Ping 200.200.200.1 à .4
* Sur le routeur dans `/etc/network/interfaces` il faut ajouter les adresses publiques des autres entreprises.  
```
auto enp0s9
    iface enp0s3 inet static
        address 200.200.200.1
        netmask 255.255.255.0
        up ip route add 2.2.2.0/24 via 200.200.200.2
        up ip route add 3.3.3.0/24 via 200.200.200.3
        up ip route add 4.4.4.0/24 via 200.200.200.4
```
* La dmz devrait pouvoir pinger les autres dmz.

### Questions
2.1- 
    Sur notre interface d'interconnexion du routeur, il faut ajouter les routes vers les autres DMZ via l'interface d'interconnexion de l'entreprise.

2.2- 
    Le réseau privé de notre entreprise, parce que même si il arrive a émettre, il est impossible pour les autres entreprise de nous répondre, car elle n'ont aucun moyen de contacter notre adresse privé. Il faudrait mettre en place un nat, pour donner une adresse publique a nos adresse privé. 

## Partie 3 Communication du réseau privé avec les serveurs web
### 3.1 réseau privé
* Pour mettre le nat en place, il suffit d'aller sur le routeur et d'utiliser iptables pour ajouter la règle : `iptables -t nat -A POSTROUTING -s 192.168.10.0/24 -o enp0s9 -j SNAT --to 200.200.200.1`

### Questions
3.1-
    Le nat ajoute des règles qui permettent de modifier les adresses et les ports des paquets, avant ou aprés le routing, selon certains critères.  Dans notre cas, on transforme aprés le routing l'adresse source de nos paquets émis par le réseau privé en l'adresse de l'interface d'interconnexion du routeur.

Fonctionnement du NAT pour une machine Linux :  
    L'objectif est de permettre à des machines d'un réseau privé de communiquer dans un réseau publique.
    Pour cela, on substitue l'adresse source du paquet (par exemple 192.168.10.10) par l'adresses publique du routeur (200.200.200.X). Le port source de la requête est aussi modifié..
    Lorsqu'une réponse revient sur le routeur, à partir du port de destination, il retrouve quelle adresse privée doit remplacer l'adresse de destination du paquet.

3.2- 
    Il est maintenant possible pour n'importe quelle machine d'accéder à toutes les adresses publiques (que ce soit du réseau DMZ-NETWORK ou bien OUR-INTERNET).
    En revanche, il n'est pas possible d'accéder aux machines du réseau PRIVATE-NETWORK depuis l'extérieur.

## Partie 4 Mise en place de la sécurité
### 4.1 Politique de sécurité
Protection du routeur :
On peut accéder au routeur en SSH depuis OUR-INTERNET ou PRIVATE-NETWORK (pour permettre de le configurer à distance).

Protection du réseau :
On peut accéder à DMZ-NETWORK depuis OUR-INTERNET ou PRIVATE-NETWORK uniquement pour le web ou DNS.

Protection des machines :
On ne peut pas accéder aux machines de PRIVATE-NETWORK ni depuis OUR-INTERNET, ni depuis DMZ-NETWORK.

### 4.2 Mise en place de la politique choisie

Voir [notre script de déploiement](./src/iptable.sh).  

Pour déployer ça plus facilement, mettre les commandes dans un script paramétrable.  
Déposer ce script sur le serveur web d'une dmz, curl/wget le et exécuter le sur chacun des routeurs.  
Les régles sont appliquées en quelques secondes !

### Questions
4.1- 
    Accepte que les connections entrante de l'extérieur sur la dmz via http  
    `iptables -A FORWARD -i <interface réseau d'interconnexion> -d <ip dmz> -p tcp --dport 80 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT`  
    Il faudra aussi mettre en place les règles autorisant les réponses de la DMZ vers l'extérieur.   
    Il faut aussi penser à la version https !  
    De même, les clients du réseau privé doivent pouvoir contacter l'extérieur.

4.2-
    On peut consulter l'état d'une connexion tcp afin de disocier les requêtes de connexions (new) des réponses (related, established).   
    Ainsi cela permet au autres de contacter notre dmz, mais interdit à notre dmz de faire autre chose que de leurs répondre. 
    
## Partie 5 Configuration automatique
### 5.1 DHCP sur réseau privé
Pour [gérer le DHCP sous debian 10](https://wiki.debian.org/fr/DHCP_Server) , on utilisera l'outils [`isc-dhcp-server`](https://packages.debian.org/search?keywords=isc-dhcp-server). 
Vu que nous ne souhaitons pas avoir un adressage dynamique au niveau de la dmz et que nous n'avons qu'un seul réseau privé, nous allons installer le serveur dhcp dans ce dernier, sur la machine routeur.  

* Installer le paquet `isc-dhcp-server`
* Editer le fichier de configuration `/etc/dhcp/dhcpd.conf` (Faire une copie avant)
* Choisir un nom de domaine `option domain-name "callune.tp";`
* Donné l'adresse du DNS option domain-name-servers <ip dns dans la dmz>;`
* Cette configuration nous dit que sur le réseau 192.168.10.0/24 on distribura dynamiquement les adresses de 192.168.10.50 à 192.168.10.200. 

        subnet 192.168.10.0 netmask 255.255.255.0 {
        range 192.168.10.50 192.168.10.200;
        option routers 192.168.10.254;
        otpion subnet-mask 255.255.255.0;
        }
* Dans `etc/default/isc-dhcp-server`rajouter l'interface d'écoute des requettes, celle lié à votre réseau privé. 
* Redemarer le service `service isc-dhcp-server restart` (un reboot sera peut être nécéssaire)
* Tester la configuration d'ip dynamique sur une machine du réseau privé. 
### Questions
5.1- 
    Le serveur DHCP doit connaître l’adresse IP du routeur, ainsi que du réseau et de l’interface qu’il doit servir.
    Il transmettra aux clients qui veulent se connecter une adresse IP générée dans la plage disponible.
    D’autres informations peuvent aussi être véhiculées, telles que l’adresse IP de la passerelle par défaut, ou encore l’adresse IP du/des serveur DNS.

5.2-   
![schema dhcp](./img/dhcp.png)

5.3-
    Ce protocole est un danger dans le cas où plusieurs serveurs sont dans le même domaine de broadcast, que ce soit dû à une attaque ou involontaire. Les deux serveurs pourront attribuer des ip possiblement en conflit.
    Un autre cas serait la panne du serveur, plus aucune machine n’aurait accès au réseau.

    Une attaque possible est le DHCP Starvation, qui consiste à envoyer plein de paquets discover, avec des adresses MAC différentes (forgées) qui paralyseront la pool disponible.
    Une attaque en lien avec celle là est le DHCP Rogue : comme le client accepte la première offre qui arrive, un attaquant peut ralentir le vrai serveur dhcp avec l’attaque du dessus, et répondre aux vrais requêtes dhcp avec ses propres informations, typiquement, mentionner que la gateway est la machine de l’attaquant, pour effectuer du man in the middle.
    Pour pallier ces défauts, une solution parmi d’autres pourrait être de chiffrer le dhcp.


## Partie 6 Mise en place d'un proxy web
### 6.1 Proxy cache web
On va utiliser [squid3](http://debian-facile.org/doc:reseau:squid-installer-un-proxy-transparent) comme proxy. On l'installera sur le routeur.  
* Intaller le paquet `squid3`. 
* Sauvegarder la conf `cp /etc/squid/squid.conf /etc/squid/squid.conf_back`
* Utiliser la conf par défault 
    ```
    echo "`grep -v "^#" /etc/squid/squid.conf | sed -e '/^$/d'`" >/etc/squid/squid.conf
    ```
* Rajouter le nom publique d'hote `visible_hostname <monhote.callune.tp>`
* Ajouter un acl pour laisser le trafique du réseau privé `acl lan src 192.168.10.0/24`
* Ajouter l'implementation de l'acl avant le deny all . `http_access allow lan`
* Redémarer le service `squid3 restart`.
* Mettre les régles de redirection du réseau privée vers le proxy dans le parfeu. 
    `iptables -t nat -A PREROUTING -s 192.168.10.0/24 -o enp0s9 -p tcp --dport 80 -j DNAT --to 127.0.0.1:3128`

### 6.2 Proxy cache transparent
* Dans `/etc/squid/squid.conf` transformer `http_port 3128 transparent`.


### Questions 
6.1- 
Un proxy web est une machine qui se place entre le client et le serveur web. Elle est vue comme le serveur de la part du client, et comme client de la part du serveur.
Son fonctionnement est simple, elle se contente de servir d'intermédiaire entre ces deux composants. Son intérêt peut être multiple ; le proxy peut par exemple service de cache pour les pages web, de cache DNS, il peut aussi inspecter les paquets qui transitent.

6.2- 
Un proxy transparent permet d'éviter la configuration par les clients web, ce qui simplifie son utilisation.

6.3- 
Mais s'il est plus simple d'utiliser un proxy transparent, il est en revanche plus compliqué à mettre en place.

## Partie 7 DNS
Nous allons mettre en place le dns dans la dmz, avec l'outils [bind9](http://debian-facile.org/atelier:chantier:dns-bind9-sur-wheezy) 

### 7.1 Espace de nommage
* Compléter `/etc/host.conf`
    ```
    order hosts, bind
    multi on
    ```
* Modifier `/etc/hosts` et déclarer son fqdn `1.1.1.1   dmz.callune.tp      dmz`
* Déclarer un nom de domaine dans `/etc/resolv.conf` 
    ```
    domain callune.tp
    search callune.tp
    nameserver 1.1.1.1
    ```
* Redémarer le service `/etc/init.d/networking start
`
### 7.2 Serveur DNS
* Installer le paquet `apt install bind9`
* Créer le fichier `/etc/bind/db.callune.tp` 
* Prendre le fichier `/etc/bind/db.local` pour modèle. 
    ```
    ;
    ; BIND data file for eth0 interface
    ;
    $TTL    604800
    @       IN      SOA     debian-serveur.mondomaine.hyp. root.mondomaine.hyp. (
                            3333         ; Serial
                            604800         ; Refresh
                            86400         ; Retry
                            2419200         ; Expire
                            604800 )       ; Negative Cache TTL
    ;
    @       IN      NS      debian-serveur.mondomaine.hyp.
    debian-serveur  IN   A  192.168.0.14
    ```
* Créer le fichier `/etc/bind/db.callune.tp.inv` 
* Prendre le fichier `/etc/bind/db.127` pour modèle. 
    ```
    ;
    ; BIND reverse data file for eth0  interface
    ;
    $TTL    604800
    @       IN      SOA     debian-serveur.mondomaine.hyp. root.mondomaine.hyp. (
                                1         ; Serial
                            604800         ; Refresh
                            86400         ; Retry
                            2419200         ; Expire
                            604800 )       ; Negative Cache TTL
    ;
    @       IN      NS      debian-serveur.
    14      IN      PTR     debian-serveur.mondomaine.hyp.
    ```
* Configurer le fichier `/etc/bind/named.conf.local` 
    ```
    //
    // Do any local configuration here
    //

    // Consider adding the 1918 zones here, if they are not used in your
    // organization
    //include "/etc/bind/zones.rfc1918";
    zone "mondomaine.hyp" {
            type master;
            file "/etc/bind/db.mondomaine.hyp";
            allow-query { any; };        
    };
    zone "0.168.192.in-addr.arpa" {
            type master;
            file "/etc/bind/db.192";
    };
    ```
* Modifier le dhcp pour qu'il fournise le dns au machine privé.
* Passer les machines privé en ip dynamiques dans `/etc/network/interfaces`
    ```
    auto enp0s3
    allow-hotplug enp0s3
    iface enp0s3 inet dhcp
    ```
* Vérifier qu'on peut curl le serveur web de la dmz avec son nom depuis le réseau privé. 
### 7.3 Service global
* Modifier `/etc/bind/named.conf.options` et rajouter l'adresse du dns responsable du domaine supérieure dans follower.
    ```
    forwarder first;
    forwarders{
        3.3.3.3;
    };
    allow-query{any;};
    auth-nxdomain no;
    dnssec-validation no;
    ```
* `systemctl restart bind9`

### Questions 
7.1- 
7.2-
