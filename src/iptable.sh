#!/bin/bash
private_network_interface="enp0s3"
dmz_network_interface="enp0s8"
intercom_network_interface="enp0s9"
dmz_web_serv_ip="1.1.1.1"
iptables_binary="/usr/sbin/iptables"

# on flush avant 
$iptables_binary -P INPUT ACCEPT
$iptables_binary -P OUTPUT ACCEPT
$iptables_binary -P FORWARD ACCEPT
$iptables_binary -F
$iptables_binary -t nat -F


# pour rendre persistent les règles il faut utiliser le paquet iptables-persistent
#apt -qq intall iptables-persistent -y && mkdir /etc/iptables && touch /etc/iptables/rules
# Le nat
iptables -t nat -A POSTROUTING -s 192.168.10.0/24 -o enp0s9 -j SNAT --to 200.200.200.1
# régle dnat pour le proxy, on redirige le trafique du réseau privé vers le trafique 
iptables -t nat -A PREROUTING -s 192.168.10.0/24 -d 200.200.200.0/24 -p tcp --dport 80 -j REDIRECT --to-ports 3129

#Le routeur fait ce qu'il veut
$iptables_binary -A INPUT -j ACCEPT
# $iptables_binary -A INPUT -p tcp --dport 53 -j ACCEPT
# accepte les paquets icmp vers le routeur de toutes les destinations
# $iptables_binary -A INPUT -p icmp -j ACCEPT

# Accepte le dhcp 
$iptables_binary -A FORWARD -p tcp --dport 67 -j ACCEPT
$iptables_binary -A FORWARD -p tcp --dport 68 -j ACCEPT

# Accepte le dns
$iptables_binary -A FORWARD -p tcp --dport 53 -j ACCEPT

# Accepte la connexion en ssh au routeur depuis notre réseau
$iptables_binary -A INPUT -i $private_network_interface -p tcp --dport 22 -m conntrack --ctstate NEW,ESTABLISHED  -j ACCEPT

# accepte d’envoyer des paquets icmp du routeur vers toutes destinations
$iptables_binary -A OUTPUT -p icmp -j ACCEPT

# accepte de transmettre des paquets icmp du routeur vers toutes destinations
$iptables_binary -A FORWARD -p icmp -j ACCEPT

# accepte d’envoyer des paquets si la connexion est établie
$iptables_binary -A OUTPUT -m conntrack ! --ctstate INVALID -j ACCEPT

# Accepte que les clients de $intercom_network_interface créer et utiliser des connections via http
$iptables_binary -A FORWARD -i $intercom_network_interface -d $dmz_web_serv_ip -p tcp --dport 80 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT

# Accepte que notre serveur web de la DMZ réponde via http
$iptables_binary -A FORWARD -s $dmz_web_serv_ip -p tcp --sport 80 -m conntrack --ctstate ESTABLISHED -j ACCEPT

# Accepte que les clients de $intercom_network_interface créer et utilise des connections via https
$iptables_binary -A FORWARD -i $intercom_network_interface -d $dmz_web_serv_ip -p tcp --dport 443 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT

# Accepte que notre serveur web de la DMZ réponde via https au client de our_internet
$iptables_binary -A FORWARD -i $dmz_network_interface -o $intercom_network_interface -s $dmz_web_serv_ip -p tcp --sport 443 -m conntrack --ctstate ESTABLISHED -j ACCEPT

# Accepte que notre serveur web de la DMZ réponde au client de $private_network_interface via http
$iptables_binary -A FORWARD -i $dmz_network_interface -o $private_network_interface -s $dmz_web_serv_ip -p tcp --sport 80 -m conntrack --ctstate ESTABLISHED -j ACCEPT

# Accepte que notre serveur web de la DMZ réponde via https au client du private_network_interface
$iptables_binary -A FORWARD -i $dmz_network_interface -o $private_network_interface -s $dmz_web_serv_ip -p tcp --sport 443 -m conntrack --ctstate ESTABLISHED -j ACCEPT

# autorise les gens du privés à passer, ils seront natés
$iptables_binary -A FORWARD -i $private_network_interface -p tcp  -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
$iptables_binary -A FORWARD -o $private_network_interface -p tcp --dport 443 -m conntrack --ctstate ESTABLISHED -j ACCEPT

# Règles qui se sont fait absorbées
    # Accepte que les clients de $private_network_interface créer et utilise des connections via https sur la DMZ
    # $iptables_binary -A FORWARD -i $private_network_interface -o $dmz_network_interface -d $dmz_web_serv_ip -p tcp --dport 443 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
    # Accepte que les clients de $private_network_interface créer et utilise des connections via http
    # $iptables_binary -A FORWARD -i $private_network_interface -o $dmz_network_interface -d $dmz_web_serv_ip -p tcp --dport 80 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT


# Par défaut, on bloque tout
$iptables_binary -P INPUT DROP
$iptables_binary -P OUTPUT DROP
$iptables_binary -P FORWARD DROP

# on sauvegarde
/sbin/iptables-save > /etc/iptables/rules.v4

